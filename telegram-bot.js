const TelegramBot = require("node-telegram-bot-api");
const token = "1501150855:AAHFAUHgtIzsC2ZJUByoTGanYg1EMh5g-H4";
const bot = new TelegramBot(token, { polling: true });
const scrapper = require("./supplementa-scrapper");
// const nodemailer = require("nodemailer");
const fs = require("fs");

var chatId;
// List of product ids, that user is subscribed to
// Contains attributes id: product url id, name and price
bot.onText(/\/article\s*$/, (msg, match) => {
  bot.sendMessage(msg.chat.id, `Which article? provide ID`);
});
// Initial usbscribe to get article price change messages
bot.onText(/\/article (.*)/, (msg, match) => {
  // 'msg' is the received Message from Telegram
  // 'match' is the result of executing the regexp above on the text content
  // of the message
  // console.log(msg, match);
  chatId = msg.chat.id;
  const resp = match[1]; // the captured "whatever"

  scrapper
    .getArticlePrice(resp)
    .then((product) => {
      if (product == undefined) {
        bot.sendMessage(chatId, `Invalid ID provided!`);
        return;
      }
      const articles = require("./articles.json");
      // send back the matched "whatever" to the chat

      bot.sendMessage(
        chatId,
        `You will get notified, when ${product.name}'s price changes. Current price: ${product.price}`
      );
      articles[match[1]] = product;
      writeArticlesFile(articles);
      // console.log(articles);
    })
    .catch((err) => {
      bot.sendMessage(chatId, `Invalid ID provided!`);
    });
});
// send email
bot.onText(/\/mail (.*)/, (msg, match) => {
  bot.sendMessage(chatId, `You will get notifications on Email: ${match[1]}`);
});

// bot.on("message", (msg) => {
//   const chatId = msg.chat.id;
//   // send a message to the chat acknowledging receipt of their message
//   bot.sendMessage(chatId, "Received your message");
// });

// async function send_email() {
//   let testAccount = await nodemailer.createTestAccount();

//   let transporter = nodemailer.createTransport({
//     service: "gmail",
//     port: 587,
//     secure: false,
//     auth: {
//       user: config.email.user,
//       pass: config.email.password,
//     },
//   });

//   // send mail with defined transport object
//   let info = await transporter.sendMail({
//     from: '"Tether.to informer" <bla@bla.com>', // sender address
//     to: "tadej.vozlic@gmail.com", // list of receivers
//     subject: "Tether.to error", // Subject line
//     text: `Getting Tether.to data was unsuccessfull \n \n ${common.get_iso_date_now()}`,
//     // html: "<b>Hello world?</b>" // html body
//   });

//   console.log("Message sent: %s", info.messageId);
//   // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

//   // Preview only available when sending through an Ethereal account
//   console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
// }

function writeArticlesFile(articles) {
  fs.writeFile(
    "articles.json",
    JSON.stringify(articles, null, 2),
    function writeJSON(err) {
      if (err) return console.log(err);
      // console.log(JSON.stringify(articles));
    }
  );
}

function checkForPriceChange() {
  // require it every time to avoid overriding changes
  const articles = require("./articles.json");
  let ids = Object.keys(articles);
  ids.forEach((id) => {
    scrapper.getArticlePrice(id).then((product) => {
      if (product.price < articles[id].price) {
        bot.sendMessage(
          chatId,
          `Discount found on product **${articles[id].name}**! Price: **${product.price}**`
        );
        articles[id] = product;
      }
    });
  });
}
setInterval(() => {
  checkForPriceChange();
}, 24 * 60 * 60 * 1000);
// 24 * 60 * 60 * 1000
