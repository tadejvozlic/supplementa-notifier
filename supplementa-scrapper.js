const axios = require("axios");
const cheerio = require("cheerio");

const BASE_URL = "https://www.supplementa.com/art/";

async function getArticlePrice(article_code) {
  try {
    const response = await axios.get(BASE_URL + article_code);
    const $ = cheerio.load(response.data);
    let content = $("#content").html();
    let name = $("h1#description").text();
    const price = $("[itemprop=price]", "span.main_price").text();
    console.log(name);
    if (name == undefined) {
      return undefined;
    }
    // console.log({
    //   name: name,
    //   price: price,
    // });
    return {
      id: article_code,
      name: name,
      price: price,
    };
  } catch (error) {
    // console.error(error);
  }
}

// function returns response text
function outpuStartingtText(name) {
  return `You will be notified when **${name}** price is changed!`;
}
// getArticlePrice("00411VY");
module.exports = { getArticlePrice };
