FROM node:14.14
ENV NODE_ENV=production

WORKDIR /dist
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
# COPY package*.json ./dist
COPY ["package.json", "package-lock.json*", "./"]


RUN npm install

COPY . .
EXPOSE 8080
CMD [ "node", "telegram-bot.js" ]
